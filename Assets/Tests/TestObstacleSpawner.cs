﻿using UnityEngine;
using System.Collections.Generic;


public class TestObstacleSpawner : MonoBehaviour {
    public GameObject[] obj;
    int startX=9;
    
	// Use this for initialization
	void Start () {
        for (int i = startX; i < startX + 40; i++)
        {
            for (int j = startX; j < startX + 40; j++)
            {
                if(Random.Range(0,20)<7)
                Instantiate(obj[Random.Range(0,obj.Length)],new Vector2(j*0.48f,i*0.48f),Quaternion.identity);
            }
        }
	}
	 
}

﻿using UnityEngine;
using System.Collections;

public class TestBombCreator : MonoBehaviour {

    public GameObject bomb;
	// Use this for initialization
	void Start () {
        Invoke("CreateBomb", 1.0f);
	}
    void CreateBomb()
    {

        Instantiate(bomb, Vector3.zero, Quaternion.identity);
    }
}

﻿using UnityEngine;
using System.Collections;
using models;
using UnityEngine.UI; 
public class SetDynaMapPosition : MonoBehaviour // this is added to MapImage
{
//    public MapPositions positions;
    public Canvas MapCanvas;
    private PlayerModel playerModel;
    void Start()
    {
        playerModel = PlayerModel.GetInstance();
        RectTransform DynaIcon = GameObject.Find("DynaIcon").GetComponent<RectTransform>();
        Vector2 lvlIconPosition = GameObject.Find("lvl " + (playerModel.currentLevel + 1).ToString()).transform.position;
        DynaIcon.position = lvlIconPosition;

        float canvasWidth = MapCanvas.pixelRect.width;
        float desiredIconXPosition = canvasWidth / 2; // fixed X center of canvas.
        float differenceX = desiredIconXPosition - lvlIconPosition.x; // how far is this center from lvl icon position? - how far must be image moved to set Dyna icon in center?
        
        float mapImageWidth = GetComponent<RectTransform>().rect.width;
        
        // If icon is too near map edge, icon can not be in the middle of screen:   <0,  - (imageWidth - canvasWidth)>
        float outOfRange = transform.position.x + differenceX;
        if (outOfRange > 0) differenceX -= outOfRange;


        float minX = -mapImageWidth + canvasWidth; // negative value. MapImage is bigger than canvas.
        
        outOfRange = (transform.position.x + differenceX); 
        if (outOfRange < minX)
        {
            float diff = minX - outOfRange; // positive value;
            differenceX += diff;
        } 
        
        transform.Translate(new Vector2(differenceX, 0));
                
        // icon.localPosition = positions.positions[playerModel.currentLevel]; // no, I don't want to use this SO any more. To many hand changes of values :/
        // icon.position = GameObject.Find("lvl " + (playerModel.currentLevel + 1).ToString()).transform.position;// positions.positions[playerModel.currentLevel];
    }
}
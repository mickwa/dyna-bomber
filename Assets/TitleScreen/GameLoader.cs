﻿using UnityEngine;
using System.Collections;
using System.IO;
using models;
public class GameLoader : MonoBehaviour {

    void Start()
    {
    }
    public void LoadGame()
    {
        Application.LoadLevel("Map");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void NewGame()
    {   
        File.Delete(Application.persistentDataPath + "/playerInfo.dat");
        PlayerModel.GetInstance().currentLevel = 0; // when starting gaim again after finishing, model is already created with data. So even after deleting file, lvl != 0
        Application.LoadLevel("Map");
    }
}

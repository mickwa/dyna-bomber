﻿using UnityEngine;
using System.Collections;
using models;
public class ObjectCollision : MonoBehaviour
{

    private GameObject[] dyna;
    private PlayerModel playerModel;
    void Start()
    {
        dyna = GameObject.FindGameObjectsWithTag("Player");
        if (dyna.Length != 1)
        {
            foreach (GameObject g in dyna)
                Debug.Log("Player: " + g.name + " pos: " + g.transform.position);
            Debug.LogError("players != 1   length: " + dyna.Length);
        }
        playerModel = PlayerModel.GetInstance();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
         if (other.tag == "enemy")
        {
            GetComponent<OnDynaKilled>().OnDamaged();   // the same thing happens on killed by bomb and by enemy;
        }
        if (other.name == "Bomb")
        {
            playerModel.canPutBomb = false;
         }

        if (other.tag == "item")
        {
            playerModel.OnItemGathered(other.gameObject);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Bomb")
        {
            playerModel.canPutBomb = true;
         }
    }
}

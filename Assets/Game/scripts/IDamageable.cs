﻿using UnityEngine;
using System.Collections;

// Each object which will be damaged by bomb must implement this
public interface IDamageable
{
     void OnDamaged();
     bool isKilled();
}

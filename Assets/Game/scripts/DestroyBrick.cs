﻿using UnityEngine;
using System.Collections;
using models;

public class DestroyBrick : MonoBehaviour,IDamageable {

    private GameObject damagedBrick;
    private BrickItemSpawner itemSpawner;
	void Start () {
        itemSpawner = GetComponent<BrickItemSpawner>();    
	}

    public void OnDamaged()
    {
        MapModel mapModel = MapModel.GetInstance();
        damagedBrick = Instantiate(Resources.Load<GameObject>("BrickDestroyed" + mapModel.worldMap), transform.position, Quaternion.identity) as GameObject;
        
        if (Random.Range(0, 10) == 2)
        {
            int itemNum = Random.Range(0, 2);
            GameObject spawnedItem = Instantiate(itemSpawner.items[itemNum], transform.position, Quaternion.identity) as GameObject;
            spawnedItem.name = itemSpawner.items[itemNum].name;
        } 
        transform.position = new Vector2(-33f, -33f);
        // forget about animation length. Just make last frame with no Sprite, stop loop and wait 2 sec.
        Invoke("OnAnimationFinish", 2f);
    }
     
    public void OnAnimationFinish()
    {
        Destroy(damagedBrick);
        Destroy(gameObject);
    }
     
    public bool isKilled()
    {
     // this might be used to not throw items each time brick is touched by explosion. (better will be removing collider)
        return false;
    }
}

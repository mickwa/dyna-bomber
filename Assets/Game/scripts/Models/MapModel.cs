﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace models
{
    public class MapModel
    {
        // Available worlds: Maze, Water, Forest, CastleFinal
        private string _worldMap = "Maze";
        public string worldMap { get {
            return _worldMap; } }
        private Dictionary<string, Sprite> worldSprites;
        private static MapModel Instance = null;
        private PlayerModel playerModel;
        public int mapHeight { get; private set; }
        public int mapWidth { get; private set; }
        
        // Positions of walls and bricks created on level start: (where not to place monsters)
        private List<Vector3> usedPositions;
        public List<Vector2> lvlSizes;

        // predefined list of Dyna and bricks positions:
        private List<GameStartPositions> startPositions;
        public int FinalBossLvl = 8;
        public int EnemyNum=3;
        private MapModel()
        {
            playerModel = PlayerModel.GetInstance();
            usedPositions = new List<Vector3>();
            CreateStartPositions();
            worldSprites = GetWorldSprites();
        }

        public static MapModel GetInstance()
        {
            if (Instance == null) Instance = new MapModel();
            return Instance;
        }

        void SetEnemyNumberForLvl()
        {
            EnemyNum = 8;// mapWidth * mapHeight / 20;
            if (playerModel.currentLevel == FinalBossLvl)
                EnemyNum = 1; // boss is also enemy
        }
        void SetWorldSize()
        {
            Debug.Log("seting world size...");
            if (playerModel.currentLevel != FinalBossLvl)
            {
                // w=5  && h=9 are smallest values that it looks good
                mapHeight = Random.Range(9, 13); //20
                mapWidth = Random.Range(5, 10); //16
            }
            else
                mapHeight = 11;
                mapWidth = 15;
         }
        public void SetWorldByLevel()
        {
            if (playerModel.currentLevel == 0 || playerModel.currentLevel == 1)
            {
                _worldMap = "Maze";
            }
            else if (playerModel.currentLevel == 2 || playerModel.currentLevel == 3)
            {
                _worldMap = "Water";
            }
            else if (playerModel.currentLevel == 4)
            {
                _worldMap = "Forest";
            }
            else
            {
                _worldMap = "CastleFinal";
            }
//            Debug.Log("Current world: " + _worldMap);
            worldSprites = GetWorldSprites();
        }

        public Dictionary<string, Sprite> GetWorldSprites()
        {
  //          Debug.Log("Getting all sprites for level: "+_worldMap);
            Dictionary<string, Sprite> sprites = new Dictionary<string,Sprite>();
            Sprite[] worldSprites = Resources.LoadAll<Sprite>(_worldMap);
            foreach (Sprite s in worldSprites)
            {  
                sprites.Add(s.name, s);
            }
            return sprites;
        }

        public Sprite GetSpriteWithName(string name)
        {
            Sprite tmp = null;
            try
            {
               tmp = worldSprites[name];
            }
            catch (KeyNotFoundException e)
            {
                Debug.Log(e.Message + " key: " + name);
            }
            return tmp;
        }
        

        public void RegisterUsedPosition(Vector3 position)
        {         
            usedPositions.Add(position);
        }

        public bool isPositionAvailable(Vector3 position)
        {
            // return !usedPositions.Any(item => position.x == item.x && position.y == item.y);
            foreach (Vector3 p in usedPositions)
            {
                if (p.x == position.x && p.y == position.y && !usedPositions.Contains(position)) { Debug.Log("impossible..."); }
            }
            return !usedPositions.Contains(position);
        }

        // called on next level.
        public void ResetModel()
        {
            SetWorldByLevel();
            usedPositions.Clear();
            SetWorldSize();
            SetEnemyNumberForLvl();
        }

        public Vector3 getRandomPositionOnMap()
        {
            int tmpX;
            int tmpY;
            int draws = 0;
            Vector3 tmpPos;
            do
            {
                tmpX = Random.Range(0, mapWidth);
                tmpY = Random.Range(0, mapHeight);
                tmpPos = new Vector3(tmpX, tmpY, 0);
                draws++;
                if (draws > 200) { Debug.LogError("draws>1000 possible no free positions. Returned position will NOT be valid!"); break; }
            } while (!isPositionAvailable(tmpPos));
            return tmpPos;
        }

        // this could be in scriptable object, but inspector is not displaying my classes. I'll add custom inspector later.
        public void CreateStartPositions()
        {
            startPositions = new List<GameStartPositions>();
            startPositions.Add(new GameStartPositions(new Vector2(2, 2), // Dyna start
                                                      new Vector2(3,3),  // bounds
                                                      new List<Vector2>() { new Vector2(0, 2), new Vector2(2, 0), new Vector2(3, 2), new Vector2(2, 3) }, // bricks
                                                      new List<Vector2>() { new Vector2(2, 1), new Vector2(1, 2), new Vector2(2, 2) })); // reserved free positions
        }

        public GameStartPositions GetDynaStartPosition()
        {
            return startPositions[0];
        }
    }
}
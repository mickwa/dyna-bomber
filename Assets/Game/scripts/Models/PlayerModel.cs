﻿using UnityEngine;
using System.Collections.Generic;
using Enums;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace models
{ 
    public class PlayerModel
    {
        private PersistentPlayerData data;
        public bool indescructible;
        private int _currentLevel;
        private static PlayerModel Instance = null;
        public float vx {get;set;}
        public float vy { get; set; }
                
        public float explosionRange = 3*0.48f+0.2f; // In units.

        public List<key> keys { get; set; }
        
        public allowedDirection allowedDirections { get; set; } // set when entering or leaving crossing

        private string playerDataFileName = Application.persistentDataPath + "/playerInfo.dat";
       

        public int currentLevel
        {
            get {
                
                return _currentLevel;
                
            }

           set
             {
                int maxLvl = 9; // NOT numbers on map. From 0 to lvl X-1
                if (value <= maxLvl) _currentLevel = value;
                else
                    _currentLevel = maxLvl;
                Debug.Log("set lvl: " + _currentLevel);
             } 
        }
        private PlayerModel()
        {
            indescructible = false;
            vx = vy= 0.05f;
            keys = new List<key>();
            canPutBomb = true;
            keys.Add(new key(KeyCode.UpArrow, allowedDirection.vertical, moveTypes.UP));
            keys.Add(new key(KeyCode.DownArrow, allowedDirection.vertical,moveTypes.DOWN));
            keys.Add(new key(KeyCode.LeftArrow, allowedDirection.horizontal,moveTypes.LEFT));
            keys.Add(new key(KeyCode.RightArrow, allowedDirection.horizontal,moveTypes.RIGHT));

            if (File.Exists(playerDataFileName))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(playerDataFileName, FileMode.Open);
                PersistentPlayerData data = (PersistentPlayerData)bf.Deserialize(file);
                currentLevel = data.level;
                explosionRange = data.explosionRange;
                file.Close();
            }
            else   // create new save game file:
            {
                currentLevel = 0;
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(playerDataFileName, FileMode.CreateNew);
                PersistentPlayerData data = new PersistentPlayerData();
                data.level = currentLevel;
                data.explosionRange = explosionRange;
                data.itemsID = new List<int>();
                bf.Serialize(file, data);
                file.Close();
            }
        }

        public void SaveGame()
        {
            Debug.Log("save game...");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(playerDataFileName, FileMode.OpenOrCreate);
            PersistentPlayerData data = new PersistentPlayerData();
            data.level = currentLevel;
            data.explosionRange = explosionRange;
            data.itemsID = new List<int>();
            bf.Serialize(file, data);
            file.Close();
        }
        public bool canPutBomb = true;
        public static PlayerModel GetInstance()
        {
            if (Instance == null) Instance = new PlayerModel();
            return Instance;
        }

        public void ResetModel()
        {
            foreach (key k in keys) k.SetDefaultValues();
            if (!Input.GetKey(KeyCode.UpArrow)) setKeyReleased(KeyCode.UpArrow);
            if (!Input.GetKey(KeyCode.DownArrow)) setKeyReleased(KeyCode.DownArrow);
            if (!Input.GetKey(KeyCode.RightArrow)) setKeyReleased(KeyCode.RightArrow);
            if (!Input.GetKey(KeyCode.LeftArrow)) setKeyReleased(KeyCode.LeftArrow);
            canPutBomb = true;
            indescructible = false;
        }
        public void setKeyPressed(KeyCode code)
        {
            (keys.Find(item => item.keycode == code)).pressed = true;
            swapMoveDirection();
        }

        public void setKeyReleased(KeyCode code)
        { 
            (keys.Find(item => item.keycode == code)).pressed = false;
            (keys.Find(item => item.keycode == code)).current = false;
            setCurrentMove();
        }

        public allowedDirection GetCurrentDirection()
        {
            key currentKey = GetCurrentKey();
            if (currentKey == null) return allowedDirection.none;
            else return currentKey.direction;
        }
        public key GetCurrentKey()
        {
            key keyToUse = null;
            if (allowedDirections == allowedDirection.any)
            {
                keyToUse = keys.Find(item => (item.allowed == true && item.pressed == true && item.colliders == 0 && item.current == true));
            }
            else
                keyToUse = keys.Find(item => (item.allowed == true && item.pressed == true && item.colliders == 0 && item.direction == allowedDirections));
            return keyToUse;
        }

        public void setCurrentMove()
        {
            List<key> allowedMoves;
            if (allowedDirections == allowedDirection.any)
                allowedMoves = keys.FindAll(item => item.pressed == true && item.allowed == true && item.colliders == 0);
            else
                allowedMoves = keys.FindAll(item => item.pressed == true && item.allowed == true && item.colliders == 0 && item.direction == allowedDirections);
             
            // remove 'current' flag from key when Dyna hits obstacle and key is still hold:
            List<key> invalidCurrentFlag = keys.FindAll(item => item.allowed == false && item.current == true);
            foreach (key k in invalidCurrentFlag) k.current = false;

            if (allowedMoves.Count == 1) allowedMoves[0].current = true;
             
        }

        public void OnLeaveCrossing() { allowedDirections = GetCurrentDirection(); }
        public void OnEnterCrossing() { 
            allowedDirections = allowedDirection.any;
            swapMoveDirection();
        }

        public void swapMoveDirection()
        {
            List<key> pressed = keys.FindAll(item => item.pressed == true && item.allowed == true && item.colliders == 0);
//            Debug.Log("allowed.count: " + pressed.Count);
            if (pressed.Count == 2)
            {
//                Debug.Log("switch direction");
                foreach (key k in pressed)
                    k.current = !k.current;
            }
        }
        public void setKey(moveTypes keyType, bool value)
        {
            if (value == false) keys[(int)keyType].colliders++;
            else keys[(int)keyType].colliders--;

            keys[(int)keyType].allowed = value; 
        }

        // if Dyna leaves wall collider, check if new move is available when is on crossing
        public void updateMoveDirection(moveTypes newAvailableMove)
        {
            if (keys[(int)newAvailableMove].pressed == true)
                swapMoveDirection();
        }

        public void setKeysDisabled()
        {
            foreach (key k in keys)
            {
                k.current = false; 
                k.allowed = false; 
            }
        }

        public void OnItemGathered(GameObject item)
        {
            if (item.name == "Increase Range") { explosionRange += 0.48f; }
            if (item.name == "Indescructability")
            {
                indescructible = true;
                GameObject.Find("Bubble").GetComponent<SpriteRenderer>().enabled = true;
               
                Debug.Log("indescructible...");
            }
            GameObject.Destroy(item);
        }
    }
}
﻿using UnityEngine;
using System.Collections.Generic;
using System;
[Serializable]
public class PersistentPlayerData {
    public int level;
    public float explosionRange;
    public List<int> itemsID;
}
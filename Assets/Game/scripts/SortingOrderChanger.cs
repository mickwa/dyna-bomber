﻿using UnityEngine;
using System.Collections;
using models;

// change order of sprite on player layer according to position.y
public class SortingOrderChanger : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private int mapHeight;
    private MapModel mapModel;
	// Use this for initialization
	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        mapModel = MapModel.GetInstance();
        mapHeight = mapModel.mapHeight;
        if (tag == "Player")
        {
            Debug.Log("h: "+mapHeight+" w: "+mapModel.mapWidth);
        }
	}
	
	// Update is called once per frame
	void Update () {
        int row = Mathf.RoundToInt(transform.position.y / 0.48f);
        //if (tag == "Player") Debug.Log(row);
	    spriteRenderer.sortingOrder = (int)(mapHeight - row )*10;
	}
}

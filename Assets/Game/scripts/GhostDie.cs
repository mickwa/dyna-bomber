﻿using UnityEngine;
using System.Collections;

// Used by each enemy. Not only Ghost
public class GhostDie : MonoBehaviour,IDamageable {

    public bool killed;
    Animator anim;
    void Start()
    {
        killed = false;
    }
    public void OnGhostAnimationFinish()
    {
        Destroy(transform.parent.gameObject);
    }

    public bool isKilled()
    {
        return killed;
    }
    public void OnDamaged()
    {

        if (transform.tag == "enemy" && isKilled() == false)
        {
            KillGhost();
            MonsterData.KilledMonster(transform.parent.name);
        }
    }
    public void KillGhost()
    {
        killed = true;
        anim = GetComponentInParent<Animator>();
        anim.SetTrigger("GhostHitTrigger");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Collider2D>().name == "col") {
            // Debug.Log("Collider death");
            OnDamaged(); }
    }
}

﻿using UnityEngine;
using System.Collections;

// Used by items to move up and down
public class Levitate : MonoBehaviour {
 
    float pos = 0;
    void Update()
    {
        pos += Time.deltaTime * 3;
        transform.Translate(new Vector2(0, Mathf.Sin(pos) / 111));
    }
}

﻿using UnityEngine;
using System.Collections;
using models;
public class OnDynaKilled : MonoBehaviour, IDamageable {

    PlayerModel playerModel;
 	void Start () {
        playerModel = PlayerModel.GetInstance();
	}
 
    public void OnDamaged()
    {
        if (playerModel.indescructible == false)
        {
            Animator anim = GetComponentInParent<Animator>();
            anim.SetBool("dieBool", true);
            // Disable move input component:

#if UNITY_EDITOR
            GetComponentInParent<WinInput2>().enabled = false;
#else
            GetComponentInParent<AndroidInput2>().enabled = false;
#endif
            playerModel.setKeysDisabled();
        }
    }

    public void OnDieAnimationFinish()
    {
        playerModel.ResetModel();
        Application.LoadLevel("game");
    }


    public bool isKilled()
    {
        return false; // not used at all. Separate interfaces.
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine.UI;
using models;

// Used to get input from android device.
public class AndroidInput2 : MonoBehaviour, IInput
{
    private PlayerModel playerModel;
    void Start()
    {
        playerModel = PlayerModel.GetInstance();
        playerModel.ResetModel();
        #if UNITY_EDITOR    
            this.enabled = false;
        #endif
     }
     
    void Update()
    {    
        UpdatePlayerInput();
    }

    public void UpdatePlayerInput()
    {
        float accX, accY;
        accX = Input.acceleration.x;
        accY = Input.acceleration.y;
        playerModel.vx = 0.05f * Mathf.Abs(accX*3);
        playerModel.vy = 0.05f * Mathf.Abs(accY*3);
        if (accX > 0.15f && playerModel.keys[(int)moveTypes.RIGHT].pressed == false) playerModel.setKeyPressed(KeyCode.RightArrow);
        if (accX < 0.09f && playerModel.keys[(int)moveTypes.RIGHT].pressed == true) playerModel.setKeyReleased(KeyCode.RightArrow);

        if (accX > -0.09f && playerModel.keys[(int)moveTypes.LEFT].pressed == true) playerModel.setKeyReleased(KeyCode.LeftArrow);
        if (accX < -0.15f && playerModel.keys[(int)moveTypes.LEFT].pressed == false) playerModel.setKeyPressed(KeyCode.LeftArrow);

        float offset = -0.0f;
        if (accY > 0.22f + offset && playerModel.keys[(int)moveTypes.UP].pressed == false) playerModel.setKeyPressed(KeyCode.UpArrow);
        if (accY < 0.08 + offset && playerModel.keys[(int)moveTypes.UP].pressed == true) playerModel.setKeyReleased(KeyCode.UpArrow);

        if (accY > -0.08f + offset && playerModel.keys[(int)moveTypes.DOWN].pressed == true) playerModel.setKeyReleased(KeyCode.DownArrow);
        if (accY < -0.22f + offset && playerModel.keys[(int)moveTypes.DOWN].pressed == false) playerModel.setKeyPressed(KeyCode.DownArrow);
        if (Input.touchCount == 2) Application.Quit();
        playerModel.setCurrentMove();
    }      
}

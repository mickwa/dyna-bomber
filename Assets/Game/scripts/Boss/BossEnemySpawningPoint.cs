﻿using UnityEngine;
using System.Collections;

public class BossEnemySpawningPoint : MonoBehaviour {

    private string[] monsterTypes = { "blue","yellow"};
		
    public void SpawnMonster()
    {
        GameObject monster = Instantiate(Resources.Load(monsterTypes[Random.Range(0,monsterTypes.Length)]),transform.position,Quaternion.identity) as GameObject;
        monster.GetComponent<EnemyMove>().ChangeMoveStrategy(new MoveDown());
        MonsterData.AddMonster();
        // set order in layer manually to get effect of exiting Boss:
        monster.GetComponent<SortingOrderChanger>().enabled = false;
        monster.GetComponent<SpriteRenderer>().sortingOrder = 53;
         // sprite order:
        // jaw = 69
        // main machine = 68
        // monster hatch = 67

        // monster inside = 62

    }
}

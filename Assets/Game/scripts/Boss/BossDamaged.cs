﻿using UnityEngine;
using System.Collections;

public class BossDamaged : MonoBehaviour,IDamageable {

    // set in inspector:
    public GameObject healthBarGreen;
    public GameObject jaw;
    public GameObject hatch; // needed to get animator
    private Animator anim;
    private float strenght = 20;

	void Start () {
        anim = hatch.GetComponent<Animator>();
	}

    public void OnDamaged()
    {
        if (isKilled() == false)
        {
            Debug.Log("ciach!!!");
            Vector2 scale = healthBarGreen.transform.localScale;
            healthBarGreen.transform.localScale = new Vector2(scale.x - (7.5f / strenght), scale.y);
            if (isKilled() == true)
            {
                anim.SetTrigger("KilledTrigger");
                jaw.transform.Rotate(0, 0, 4);
                MonsterData.KilledMonster(transform.parent.name);
                Debug.Log("Boss killed!");
            }
            else
            {
                Debug.Log("not yet...");
            }
        }
    }

    public bool isKilled()
    {
        return (healthBarGreen.transform.localScale.x < 0.01f);
    }
}
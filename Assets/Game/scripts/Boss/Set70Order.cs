﻿using UnityEngine;
using System.Collections;

// This is used to set order in player layer to 70 so enemy will be over jaw.

// ok, I know this is bad, but I don't have better idea, and I wasn't even thinking about any final boss when starting coding right? ;)
public class Set70Order : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "enemy")
        {
            SpriteRenderer sr = other.GetComponentInParent<SpriteRenderer>();
            sr.sortingOrder = 70;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "enemy")
        {
            SortingOrderChanger soc = other.GetComponentInParent<SortingOrderChanger>();
            soc.enabled = true;
        }
    }
}

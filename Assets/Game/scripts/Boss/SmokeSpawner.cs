﻿using UnityEngine;
using System.Collections;

public class SmokeSpawner : MonoBehaviour {

    // set in inspector:
    public GameObject SmokePrefab;
    public bool puffLeft; // make smoke left or right?
    public GameObject BossCollider; // to check isKilled();
    
    private bool AllTheTime;
    private Vector2 scale;
    private GameObject smoke;
    private BossDamaged bossDamaged;
	void Start () {
        bossDamaged = BossCollider.GetComponent<BossDamaged>();
        if (Random.Range(0, 5) == 2) AllTheTime = true;
        if (AllTheTime)
        {
            InvokeRepeating("CreateSmoke", 0, Random.Range(0.4f, 1f));
            float val = Random.Range(0.2f,2.5f);
            scale = new Vector2(val, val);
        }
        else
            InvokeRepeating("CreateSmoke", 0, Random.Range(0.7f, 4f));
	}

    private void CreateSmoke()
    {
        if (bossDamaged.isKilled() == true)
        {
            CancelInvoke("CreateSmoke");
        }

        if (smoke != null) Destroy(smoke);
        if (Random.Range(0, 3) == 2 || AllTheTime)
        {
            smoke = Instantiate(SmokePrefab, transform.position, Quaternion.identity) as GameObject;
            smoke.transform.parent = transform;
            if (puffLeft)
                smoke.transform.Rotate(new Vector3(0, 180, 0));
            if (!AllTheTime)
            {
                float val = Random.Range(0.3f, 2.5f);
                scale = new Vector2(val, val);
            }
            smoke.transform.localScale = scale;
        }
    }
}

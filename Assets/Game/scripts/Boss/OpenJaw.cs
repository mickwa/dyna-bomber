﻿using UnityEngine;
using System.Collections;

public class OpenJaw : MonoBehaviour {

    private Animator anim;
 	void Start () {
        anim = GetComponent<Animator>();
        WaitForHatchUp();
	}

    void WaitForHatchUp()
    {
        Invoke("MoveHatchUp", Random.Range(1.7f, 3));
    }
	void MoveHatchUp() {
        anim.SetTrigger("HatchUpTrigger");
        Invoke("MoveHatchDown", 3f);
	}
    void MoveHatchDown()
    {
        anim.SetTrigger("HatchDownTrigger");
        WaitForHatchUp();
    }
}

﻿using UnityEngine;
using System.Collections;

public class BossSpawnEnemies : MonoBehaviour {

    // set in inspector:
    public GameObject insideBoss;

    public void SpawnMonsters()
    {
        BossEnemySpawningPoint[] points = insideBoss.GetComponentsInChildren<BossEnemySpawningPoint>();
        foreach (BossEnemySpawningPoint p in points)
            p.SpawnMonster();
    }
}

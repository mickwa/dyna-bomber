﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
// used on map to show scroll area
public class RectGizmo : MonoBehaviour {
    void OnDrawGizmos()
    {
        RectTransform rt = GetComponent<RectTransform>();
        Gizmos.color = new Color(1f, 1f, 0f, 0.4f);
        Gizmos.DrawCube(transform.position, rt.rect.size);
    }
}

﻿using UnityEngine;
using System.Collections;
using models;
public class TeleportDynaToNextLevel : MonoBehaviour {
     
    public GameObject TeleportingDynaCanvas; 
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.transform.name == "ObjectCollider")  // ;(
        {
            PlayerModel playerModel = PlayerModel.GetInstance();
            playerModel.currentLevel++;
            playerModel.SaveGame();
            Debug.Log("teleport");
            Instantiate(TeleportingDynaCanvas, transform.position, Quaternion.identity);
            Destroy(other.transform.parent.gameObject);
        }

    }
}

﻿using UnityEngine;
using System.Collections;
using models;
public class GoToLevel : MonoBehaviour {

    public void StartGame()
    {
        MapModel.GetInstance().SetWorldByLevel();
        Application.LoadLevel("game");
    }
}

﻿using UnityEngine;
using Enums;
public interface IMoveStrategy{

    moveTypes GetNextMove(Transform t);
}



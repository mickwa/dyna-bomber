﻿using UnityEngine;
using System.Collections;

public class MoveDown:IMoveStrategy
{

    public Enums.moveTypes GetNextMove(Transform t)
    {
        return Enums.moveTypes.DOWN;
    }
}

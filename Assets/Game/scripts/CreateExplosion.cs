﻿using UnityEngine;
using System.Linq;
using models;
using System.Collections.Generic;
public class CreateExplosion : MonoBehaviour {

    public GameObject bombBack;
    public GameObject bombMiddle;
    public GameObject bombEnd;
    private PlayerModel playerModel;

    RaycastHit2D[] hits;
    float range = 4*0.48f; // in editor if no model available

    private RaycastHit2D[] GetCollidedOrderedObjects(Vector2 dir)
    {
        return Physics2D.RaycastAll(transform.position, dir, range).OrderBy(h => Vector2.Distance(transform.position, h.transform.position)).ToArray();
    }
    void Start()
    {
        Physics2D.queriesHitTriggers = true;    
            
        playerModel = PlayerModel.GetInstance();
        range = playerModel.explosionRange;
        List<Vector2> dirs = new List<Vector2> {Vector2.right,  -Vector2.up, -Vector2.right, Vector2.up };
        
        GameObject tmp = Instantiate(bombMiddle, transform.position, Quaternion.identity) as GameObject;
        tmp.transform.parent = transform;
         
        foreach (Vector2 dir in dirs)
        {
            hits = GetCollidedOrderedObjects(dir);
             
            float blockerRange = range;
            RaycastHit2D Blocker = hits.FirstOrDefault(h => h.transform.tag == "wall" || h.transform.tag == "brick");

            if (Blocker.collider != null)
            {
                blockerRange = Vector2.Distance(transform.position, Blocker.point);
            }
             
            RaycastHit2D[] toDestroy;
            toDestroy = hits.Where(h => Vector2.Distance(transform.position, h.point) <= blockerRange).ToArray();
               
            foreach (RaycastHit2D hit in toDestroy)
            {
              //  IDamageable damagable = hit.transform.GetComponent(typeof(IDamageable)) as IDamageable;    // !!!!!! Great!
                IDamageable damagable = hit.transform.GetInterfaceComponent<IDamageable>(); // my extension is better :D
                if (damagable != null)   
                {
                    if (damagable.isKilled() == false)
                    {
                        damagable.OnDamaged();
                        
                    }
                }
            }

            
            // create collider:
            GameObject colGO = new GameObject("col", typeof(BoxCollider2D));
            colGO.GetComponent<Collider2D>().isTrigger = true;

            // instantiate explosion gfx:
            if(dir == Vector2.up)
            {
                colGO.transform.position = new Vector2(transform.position.x, transform.position.y + blockerRange/2);
                colGO.transform.localScale = new Vector2(0.3f, blockerRange);
                colGO.transform.SetParent(transform);
                GameObject tmpExplosion;
                for (float i = transform.position.y + 0.48f; i < transform.position.y + blockerRange; i += 0.48f)
                {

                    if (i < transform.position.y + blockerRange - 0.48f || blockerRange != playerModel.explosionRange)
                    {
                        tmpExplosion = Instantiate(bombBack, new Vector2(transform.position.x, i), Quaternion.identity) as GameObject;
                    }
                    else
                    {
                        tmpExplosion = Instantiate(bombEnd, new Vector2(transform.position.x,i), Quaternion.identity) as GameObject;
                    }
                    tmpExplosion.transform.parent = transform;
                }
            }
            
            if(dir == -Vector2.up)
            {
                colGO.transform.position = new Vector2(transform.position.x, transform.position.y - blockerRange / 2);
                colGO.transform.localScale = new Vector2(0.3f, blockerRange);
                colGO.transform.SetParent(transform);
                GameObject tmpExplosion;
                for (float i = transform.position.y - 0.48f; i > transform.position.y - blockerRange; i -= 0.48f)
                {
                    if (i > transform.position.y - blockerRange + 0.48f || blockerRange != playerModel.explosionRange)
                    {
                        tmpExplosion = Instantiate(bombBack, new Vector2(transform.position.x, i), Quaternion.identity) as GameObject;
                    }
                    else
                    { 
                        tmpExplosion = Instantiate(bombEnd, new Vector2(transform.position.x, i), Quaternion.identity) as GameObject;
                        tmpExplosion.transform.Rotate(new Vector3(0, 0, 180f));
                    }
                    tmpExplosion.transform.parent = transform;
                }
            }

             if(dir == Vector2.right) 
            {
                colGO.transform.position = new Vector2(transform.position.x + blockerRange / 2, transform.position.y);
                colGO.transform.localScale = new Vector2(blockerRange,0.3f);
                colGO.transform.SetParent(transform);
                for (float i = transform.position.x + 0.48f; i < transform.position.x + blockerRange; i += 0.48f)
                {
                    GameObject tmpExplosion;
                    if (i < transform.position.x + blockerRange - 0.48f || blockerRange != playerModel.explosionRange)
                    {
                        tmpExplosion = Instantiate(bombBack, new Vector2(i, transform.position.y), Quaternion.identity) as GameObject;
                        tmpExplosion.transform.Rotate(new Vector3(0, 0, 90f));
                    }
                    else
                    {
                        tmpExplosion = Instantiate(bombEnd, new Vector2(i, transform.position.y), Quaternion.identity) as GameObject;
                        tmpExplosion.transform.Rotate(new Vector3(0, 0, 270f));
                    }
                    tmpExplosion.transform.parent = transform;
                }
            }
             if (dir == -Vector2.right)
             {
                 colGO.transform.position = new Vector2(transform.position.x - blockerRange / 2, transform.position.y);
                 colGO.transform.localScale = new Vector2(blockerRange, 0.3f);
                 colGO.transform.SetParent(transform);
                 GameObject tmpExplosion;
                 for (float i = transform.position.x - 0.48f; i > transform.position.x - blockerRange; i -= 0.48f)
                 {
                     if (i > transform.position.x - blockerRange + 0.48f || blockerRange != playerModel.explosionRange)
                     {
                         tmpExplosion = Instantiate(bombBack, new Vector2(i, transform.position.y), Quaternion.identity) as GameObject;
                         tmpExplosion.transform.Rotate(new Vector3(0, 0, 90f));
                     }
                     else
                     {
                         tmpExplosion = Instantiate(bombEnd, new Vector2(i, transform.position.y), Quaternion.identity) as GameObject;
                         tmpExplosion.transform.Rotate(new Vector3(0, 0, 90f));
                     }
                     tmpExplosion.transform.parent = transform;
                 }
             }
        }
    }
     
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

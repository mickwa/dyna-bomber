﻿using UnityEngine;
using System.Collections;
using models;
// This is used by stars animation on level exit when Dyna is teleporting to next level
// 
public class FollowMask : MonoBehaviour {

    public RectTransform mask;
	
	// Update is called once per frame
	void Update () {
        transform.localPosition = new Vector2(transform.localPosition.x, mask.transform.localPosition.y+0.20f);
        if (transform.localPosition.y < -0.15f)
        {
            gameObject.GetComponent<RectTransform>().transform.position = new Vector2(-100f, 100f);
            Invoke("GoToMap", 2f);
        }
	}
    void GoToMap()
    {
        if (PlayerModel.GetInstance().currentLevel == 9)
            Application.LoadLevel("EndScene");
        else
        Application.LoadLevel("Map");
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enums;
using models;

public class crossingEnter : MonoBehaviour {
    private PlayerModel playerModel;

    void Start () {
       playerModel = PlayerModel.GetInstance();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
         if (other.name == "cross") playerModel.OnEnterCrossing();
    }

	void OnTriggerExit2D(Collider2D other)
	{
        if (other.name == "cross") playerModel.OnLeaveCrossing();
	}
}

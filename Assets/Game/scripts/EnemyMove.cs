﻿using UnityEngine;
using System.Collections;
using Enums;
public class EnemyMove : MonoBehaviour {

    private IMoveStrategy moveStrategy = null;
    Vector2 lastPosition;
    moveTypes move;
    float progress = 0;

	void Start()
    {
     // if move strategy was change while creating monster it won't be null.
        if(moveStrategy == null) moveStrategy = new RandomMove();
        lastPosition = transform.position;
        move = moveStrategy.GetNextMove(transform);

    }
      
    void Update()
    {
        if (progress >= 1)
        {
            progress = 0;
            transform.position = lastPosition + Globals.vectors[move]*0.48f;
            lastPosition = transform.position;
            move = moveStrategy.GetNextMove(transform);
        }
        transform.position = Vector2.Lerp(lastPosition, lastPosition + Globals.vectors[move]*0.48f,progress);
        progress += 0.48f*5 * Time.deltaTime;
    }

    // When instantiating and using this function at once, this function is executed before Start!
    public void ChangeMoveStrategy(IMoveStrategy newStrategy)
    {
//        Debug.Log("change strategy");
        moveStrategy = newStrategy;
    }
}

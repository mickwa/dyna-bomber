﻿using UnityEngine;
using System.Collections;

public class explode : MonoBehaviour,IDamageable {
	public GameObject explosion;
	 
	void Start () {
		Invoke("bombExplode",2.8f);
 	}
	  
	void bombExplode()
	{
//        Debug.Log("bomb explode");
        Vector2 explosionSpawnPosition = transform.position;
        StartCoroutine("ChangeBombPosition");
        Instantiate(explosion,explosionSpawnPosition ,Quaternion.identity);
		
 	}

    // when explosion of other bomb touches this bomb, cancel waiting for its explosion and explode now:
    public void OnDamaged()
    {
        CancelInvoke("bombExplode");
        bombExplode();
    }

    public bool isKilled() {
        return false; }
    IEnumerator ChangeBombPosition()
    {
        transform.position = Vector2.right * (-100); // move this far away, so OnTriggerExit2D() will be triggered
        yield return null;
        Destroy(gameObject); 
        yield break;
    }
}

﻿using UnityEngine;
using System.Collections;

public class PassAnimEvent : MonoBehaviour {

    public void OnGhostAnimationFinish()
    {
        GhostDie gd = GetComponentInChildren<GhostDie>();
        gd.OnGhostAnimationFinish();
    }
}
 

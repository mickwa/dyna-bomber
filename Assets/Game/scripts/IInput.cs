﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enums;
public interface IInput
{
    void UpdatePlayerInput();
}

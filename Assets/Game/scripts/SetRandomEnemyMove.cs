﻿using UnityEngine;
using System.Collections;

public class SetRandomEnemyMove : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "enemy")
        {
            other.GetComponentInParent<EnemyMove>().ChangeMoveStrategy(new RandomMove());
        }
    }
}

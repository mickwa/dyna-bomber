﻿using UnityEngine;
using System.Collections;
using models;
public class OnGetItem : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(GetComponent<Collider2D>().name);
        if (GetComponent<Collider2D>().name == "Increase Range" && other.name == "ObjectCollider")
        {
            PlayerModel.GetInstance().explosionRange+=0.48f;
            Destroy(gameObject);
        }
 	}
}

﻿using UnityEngine;
using System.Collections;
using Enums;
using UnityEngine.UI;
using models;
public class WallCollisionDetector : MonoBehaviour {
    PlayerModel playerModel;
    void Start()
    {
        playerModel = PlayerModel.GetInstance();
    }
	 
    void OnTriggerEnter2D(Collider2D other)
    {  
        if (other.tag == "wall" || other.tag == "brick")
        {
  //          Debug.Log("[enter]  Collider: "+name+"  wall: " + other.name);
            if (playerModel == null) { Debug.Log("playerModel == null: "+Time.frameCount+" oname: "+other.transform.name +" name: "+ transform.name); return; }

            if (name == "UpWallCollider") playerModel.setKey(moveTypes.UP, false);
            if (name == "DownWallCollider") playerModel.setKey(moveTypes.DOWN, false);
            if (name == "LeftWallCollider") playerModel.setKey(moveTypes.LEFT, false);
            if (name == "RightWallCollider") playerModel.setKey(moveTypes.RIGHT, false);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "wall" || other.tag == "brick")
        {
//            Debug.Log("[leave]  Collider: " + name + "  wall: " + other.name);
            moveTypes toCheck = moveTypes.UNDEFINED;
            if (name == "UpWallCollider") toCheck = moveTypes.UP;
            if (name == "DownWallCollider")toCheck = moveTypes.DOWN;
            if (name == "LeftWallCollider") toCheck = moveTypes.LEFT;
            if (name == "RightWallCollider") toCheck = moveTypes.RIGHT;
            if (toCheck == moveTypes.UNDEFINED) Debug.LogError("wrong to check value");

            playerModel.setKey(toCheck, true);
            playerModel.updateMoveDirection(toCheck);
        }
    }

}

﻿using UnityEngine;
using System.Collections;

public static class Extension { 
public static T GetInterfaceComponent<T>(this Transform t) where T : class // this works with interfaces... 
{
    return t.GetComponent(typeof(T)) as T;
}
}

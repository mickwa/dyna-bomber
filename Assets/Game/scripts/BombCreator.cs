﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using models;

// allows Dyna to put bombs on map
public class BombCreator : MonoBehaviour {
	 
	public GameObject bomb;
    private PlayerModel playerModel;
    void Start()
    {
        playerModel = PlayerModel.GetInstance();
    }
	void Update () {
        #if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Space))
        #else
            if(Input.touchCount == 1)
        #endif
            {
                if (playerModel.canPutBomb == true)
                {
                    Vector2 bombPosition = new Vector2(Mathf.RoundToInt(transform.position.x / 0.48f) * 0.48f, Mathf.RoundToInt(transform.position.y / 0.48f) * 0.48f);
                    GameObject bombTmp = Instantiate(bomb, bombPosition, Quaternion.identity) as GameObject;
                    bombTmp.name = bomb.name;
                }
                else
                {
                    Debug.Log("Can't put bomb");
                }
        } 
	}
}
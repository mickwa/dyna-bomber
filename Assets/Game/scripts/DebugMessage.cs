﻿using UnityEngine;
using System.Collections;
using models;
using UnityEngine.UI;

public class DebugMessage : MonoBehaviour {

    private PlayerModel playerModel;
    private Text debugText;
	// Use this for initialization
	void Start () {
        playerModel = PlayerModel.GetInstance();
        debugText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        debugText.text = "";
/*
        debugText.text ="vx: "+ playerModel.vx+" vy: "+playerModel.vy;
           */
        foreach (key k in playerModel.keys)
             debugText.text += k.ToString() + "\n" ;
        debugText.text += playerModel.allowedDirections.ToString();

         
	}
}

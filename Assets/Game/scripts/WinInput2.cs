﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine.UI;
using models;

// take input on windows
public class WinInput2 : MonoBehaviour, IInput {
     
    private PlayerModel playerModel;

	void Start () {
    #if !UNITY_EDITOR
        this.enabled = false;
    #endif
        playerModel = PlayerModel.GetInstance();
        playerModel.ResetModel();
    }
	
	void Update () {
        UpdatePlayerInput();
    }

    public void UpdatePlayerInput()
    {
        KeyCode[] toCheck = { KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.UpArrow, KeyCode.DownArrow };

        foreach (KeyCode code in toCheck)
        {
            if (Input.GetKeyDown(code)) playerModel.setKeyPressed(code);
            if (Input.GetKeyUp(code)) playerModel.setKeyReleased(code);
        }
        playerModel.setCurrentMove();
    }
     
    public void stopDyna() {
        playerModel.setKeysDisabled();
    }
}
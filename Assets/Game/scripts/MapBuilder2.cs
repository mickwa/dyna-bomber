﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using models;
using System.Linq;
public class MapBuilder2 : MonoBehaviour
{

    // set in inspector. They are used, only Sprites are changes.
    public GameObject grass;
    public GameObject grassWithShadow;
    public GameObject wall;
    public GameObject brick;
    public GameObject crossing;
    public GameObject levelExit;

    private MapModel mapModel;
    private PlayerModel playerModel;
    private int FinalBossLvl;
    private const float size = 0.48f;

    private bool DynaSafeSpawnType; // random or safe. If game borard is too small for any safe spawn pattern use random spawn point.

    void Awake()
    {
        playerModel = PlayerModel.GetInstance();
        if (playerModel.currentLevel == 9) Application.LoadLevel("EndScene");
        mapModel = MapModel.GetInstance();
        mapModel.ResetModel();
    }
    void Start()
    {
        // reset monster manager:
        MonsterData.ResetData();
        SetMonstersToSpawn();
        
        FinalBossLvl = mapModel.FinalBossLvl;
        // in future not 3 but selected 
        DynaSafeSpawnType = (mapModel.mapHeight > 3 && mapModel.mapWidth > 3) ? true : false;
      
        // safe spawning can be done only when game board is big enought, >= 
        if (DynaSafeSpawnType && playerModel.currentLevel != FinalBossLvl) SpawnDyna2();
        #region buildingMap
        for (int i = 0; i < mapModel.mapWidth; i++)
        {
            for (int j = 0; j < mapModel.mapHeight; j++)
            {
                #region CreateGrass
                if (j % 2 == 0 && i % 2 == 1 && j != mapModel.mapHeight - 1)
                {
                    GameObject tmp;
                    tmp = Instantiate(grassWithShadow, new Vector3(i * size, j * size, 0), Quaternion.identity) as GameObject;
                    tmp.GetComponent<SpriteRenderer>().sprite = mapModel.GetSpriteWithName("GrassWithShadow");
                    tmp.transform.parent = transform;
                }
                else
                {
                    GameObject tmp;
                    tmp = Instantiate(grass, new Vector3(i * size, j * size, 0), Quaternion.identity) as GameObject;
                    tmp.GetComponent<SpriteRenderer>().sprite = mapModel.GetSpriteWithName("Grass");
                    tmp.transform.parent = transform;
                }
                #endregion
                #region CreteWalls
                if (j % 2 == 1 && i % 2 == 1)
                {
                    GameObject newWall;
                    newWall = Instantiate(wall, new Vector3(i * size, j * size, 0), Quaternion.identity) as GameObject;
                    //                    Debug.Log("Adding: x" + i + "  y: " + j+"name: "+tmp.name);
                    mapModel.RegisterUsedPosition(new Vector3(i, j, 0));
                    newWall.transform.parent = transform;

                    // set sprite:
                    string wallName;
                    if (mapModel.worldMap == "CastleFinal")
                        wallName = "Wall" + Random.Range(0, 3);
                    else
                        wallName = "Wall";
                    newWall.GetComponent<SpriteRenderer>().sprite = mapModel.GetSpriteWithName(wallName);
                    SpriteRenderer sr = newWall.GetComponent<SpriteRenderer>();
                    sr.sortingOrder = (mapModel.mapHeight - j) * 10;

                    newWall.name = sr.sortingOrder.ToString();

                    
                }
                #endregion
                #region CreateCrossings
                if (j % 2 == 0 && i % 2 == 0)
                {
                    GameObject tmp;
                    tmp = Instantiate(crossing, new Vector3(i * size, j * size, 0), Quaternion.identity) as GameObject;
                    tmp.name = crossing.name;
                }
                #endregion


            }
        }
        #endregion // buildingMap

        // spawn enemies:
        if (playerModel.currentLevel == FinalBossLvl) // Final Boss :D
        {
            GameObject DynaBomber = GameObject.FindGameObjectWithTag("Player");
            DynaBomber.transform.position = new Vector2(6f, 2f) * size;
            Instantiate(Resources.Load("FinalBoss"), new Vector2(8.623f, 2.23f), Quaternion.identity);
            MonsterData.AddMonster();
        }
        else
        {
            if (!DynaSafeSpawnType) SpawnDyna();
            SpawnBricks();
            SpawnMonsters();
        }
    }

    private Dictionary<string, List<string>> monsters = new Dictionary<string, List<string>>();
    void SetMonstersToSpawn()
    {
        monsters.Add("Maze", new List<string>() { "Yellow" });
        monsters.Add("Water", new List<string>() { "Blue", "Croc" });
        monsters.Add("Forest", new List<string>() { "Blue", "Ghost" });
        monsters.Add("CastleFinal", new List<string>() { "Yellow", "Ghost" });
    }

    void SpawnMonsters()
    {
        int monstersToSpawn = mapModel.EnemyNum;
        GameObject newMonster;
        while (monstersToSpawn > 0)
        {
            Vector3 tmpPos = mapModel.getRandomPositionOnMap();
            GameObject MonsterToSpawn = Resources.Load(monsters[mapModel.worldMap][Random.Range(0, monsters[mapModel.worldMap].Count)], typeof(GameObject)) as GameObject;

            newMonster = Instantiate(MonsterToSpawn, tmpPos * size, Quaternion.identity) as GameObject;
            newMonster.name = MonsterToSpawn.name + "_" + monstersToSpawn;

            monstersToSpawn--;
            MonsterData.AddMonster();
            mapModel.RegisterUsedPosition(tmpPos);
        }
    }


    void SpawnBricks()
    {
        for (int i = 0; i < mapModel.mapWidth * mapModel.mapHeight / 6; i++)
        {
            Vector3 tmpPos = mapModel.getRandomPositionOnMap();
            GameObject newBrick = Instantiate(brick, tmpPos * size, Quaternion.identity) as GameObject;
            SpriteRenderer tmpSpriteRen = newBrick.GetComponent<SpriteRenderer>();
            tmpSpriteRen.sprite = mapModel.GetSpriteWithName("Brick");
            tmpSpriteRen.sortingOrder = (int)(mapModel.mapHeight - tmpPos.y) * 10;
            mapModel.RegisterUsedPosition(tmpPos);
            newBrick.transform.parent = transform;
            newBrick.name = "Brick " + tmpSpriteRen.sortingOrder;
        }
    }
    // It looks like BoxCollider can be created (always is?) before Start() functions of 
    // some comnponents, and OnTriggerEnter2D() is executed before Start() in some GOs using models, Dyna can not be spawned he must be moved...
    // but there are problems with allwedDirections when Dyna changes place. 
    private void SpawnDyna()
    {
        GameObject DynaBomber = GameObject.FindGameObjectWithTag("Player");
        RaycastHit2D[] bricksOrWalls;
        Vector3 tmpPos;
        int draws = 0;
        // if map is too small, condition of safe position will not be found. 
        do
        {

            tmpPos = mapModel.getRandomPositionOnMap();
            // check if this position is safe to put bomb:
            Physics2D.queriesHitTriggers = true;
            bricksOrWalls = Physics2D.RaycastAll(tmpPos * 0.48f, Vector3.right, 4 * 0.48f);
            if (draws++ > 222) { Debug.LogWarning("No safe place for Dyna found"); break; }
             //          foreach (RaycastHit2D hit in bricksOrWalls) Debug.Log(hit.transform.name+"tag: "+hit.transform.tag);
        }
        while (bricksOrWalls.Where(item => item.collider.tag == "brick" || item.collider.tag == "wall").ToArray().Length != 0);
        //        Debug.Log("Dyna: " + tmpPos);     
        DynaBomber.transform.position = tmpPos * size;
        if (tmpPos.x % 2 == 0) PlayerModel.GetInstance().allowedDirections = Enums.allowedDirection.vertical;
        if (tmpPos.y % 2 == 0) PlayerModel.GetInstance().allowedDirections = Enums.allowedDirection.horizontal;
        if (tmpPos.x % 2 == 0 && tmpPos.y % 2 == 0) PlayerModel.GetInstance().allowedDirections = Enums.allowedDirection.any;
    }

    private void SpawnDyna2()
    {
        GameObject DynaBomber = GameObject.FindGameObjectWithTag("Player");
        // vector2 components must be even, or bricks will collide with walls
        Vector2 shift = new Vector2(Random.Range(0, (mapModel.mapWidth - 3) / 2), Random.Range(0, (mapModel.mapHeight - 3) / 2));
        shift *= 2;
        GameStartPositions startPos = mapModel.GetDynaStartPosition();

        DynaBomber.transform.position = (startPos.DynaStartPosition + shift) * size;
        if ((startPos.DynaStartPosition + shift).x % 2 == 0) PlayerModel.GetInstance().allowedDirections = Enums.allowedDirection.vertical;
        if ((startPos.DynaStartPosition + shift).y % 2 == 0) PlayerModel.GetInstance().allowedDirections = Enums.allowedDirection.horizontal;
        if ((startPos.DynaStartPosition + shift).x % 2 == 0 &&
            (startPos.DynaStartPosition + shift).y % 2 == 0) PlayerModel.GetInstance().allowedDirections = Enums.allowedDirection.any;

        foreach (Vector2 pos in startPos.BrickPositions)
        {
            GameObject newBrick = Instantiate(brick, (pos + shift) * size, Quaternion.identity) as GameObject;
            SpriteRenderer tmpSpriteRen = newBrick.GetComponent<SpriteRenderer>();
            tmpSpriteRen.sprite = mapModel.GetSpriteWithName("Brick");
            tmpSpriteRen.sortingOrder = (int)(mapModel.mapHeight - (pos + shift).y) * 10;
            
            mapModel.RegisterUsedPosition(pos + shift);
            newBrick.transform.parent = transform;

            newBrick.name = "brick " + tmpSpriteRen.sortingOrder;

        }

        foreach (Vector2 pos in startPos.EmptyReservedPositions)
        {
            mapModel.RegisterUsedPosition(pos + shift);
        }
    }

    public void SpawnLevelExit()
    {
        Debug.Log("level clear, spawning exit...");
        Vector2 exitPosition;
        if (playerModel.currentLevel == FinalBossLvl)
            exitPosition = new Vector2(7f, 2f);
        else
            exitPosition = mapModel.getRandomPositionOnMap();
        Instantiate(levelExit, exitPosition * 0.48f, Quaternion.identity);
    }
}
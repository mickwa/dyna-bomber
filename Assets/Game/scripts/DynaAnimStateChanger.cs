﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using models;
using Enums;

// change animations when input is gathered from keybord OR android device
 public class DynaAnimStateChanger : MonoBehaviour {
	private Animator anim;
    private PlayerModel playerModel;
     
	void Start()
    {
        playerModel = PlayerModel.GetInstance();
        
  		anim = GetComponentInChildren<Animator>();
		anim.SetBool("front", true);
		anim.SetBool("idleVertical", true);
		anim.SetBool("right", true);
        anim.SetBool("idleHorizontal", true);
      }
       
	void Update() 
	{
        key currentKey = playerModel.GetCurrentKey();

		if(currentKey == null)
		{
			anim.SetBool("idleVertical",true);
			anim.SetBool("idleHorizontal",true);
			// right and front should be ok from last moving state.
		}
		 
		else // so at least one key is still hold.
		{ 
			if(currentKey.keycode == KeyCode.RightArrow)
			{ 
			 	anim.SetBool("right",true);
				anim.SetBool("idleHorizontal",false);
				anim.SetBool("idleVertical",true);
                if (playerModel.allowedDirections != allowedDirection.vertical) anim.CrossFade("walkRight", 0);
			}

			if(currentKey.keycode == KeyCode.LeftArrow)
			{ 
				anim.SetBool("right",false);
				anim.SetBool("idleHorizontal",false); 
				anim.SetBool("idleVertical",true);
                if (playerModel.allowedDirections != allowedDirection.vertical) anim.CrossFade("walkLeft", 0); 
			}
			  
			if(currentKey.keycode == KeyCode.UpArrow)
			{
				anim.SetBool("front",false);
				anim.SetBool("idleVertical",false);
				anim.SetBool("idleHorizontal",true);
                if (playerModel.allowedDirections != allowedDirection.horizontal) anim.CrossFade("walkBack", 0); 
			}
			
			if(currentKey.keycode == KeyCode.DownArrow)
			{
				anim.SetBool("front",true);
				anim.SetBool("idleVertical",false);
				anim.SetBool("idleHorizontal",true);
                if (playerModel.allowedDirections != allowedDirection.horizontal) anim.CrossFade("walkFront", 0); 
			} 
		
		 
		if((currentKey.keycode == KeyCode.RightArrow || currentKey.keycode == KeyCode.LeftArrow) &&
           (playerModel.allowedDirections == allowedDirection.horizontal || playerModel.allowedDirections == allowedDirection.any))
			{
				transform.Translate(new Vector3(playerModel.vx*((anim.GetBool("right") == true)?1:-1),0,0));
				transform.position = new Vector3(transform.position.x,transform.position.y -Deviation(transform.position.y)/8.0f, transform.position.z);
			}

		if(  (currentKey.keycode == KeyCode.UpArrow || currentKey.keycode == KeyCode.DownArrow) &&
           (playerModel.allowedDirections == allowedDirection.vertical || playerModel.allowedDirections == allowedDirection.any))
			{
				transform.Translate(new Vector3(0,playerModel.vy*((anim.GetBool("front") == true)?-1:1),0)); 
				transform.position = new Vector3(transform.position.x -Deviation(transform.position.x)/8.0f,transform.position.y, transform.position.z);
			}
        }
	} 
      
	float Deviation(float toCheck, float norm=0.96f)
	{
		float rest = toCheck % norm;
		if(rest > norm/2) return (rest - norm);
		return rest;
	}
}
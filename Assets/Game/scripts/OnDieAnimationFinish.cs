﻿using UnityEngine;
using System.Collections;

public class OnDieAnimationFinish : MonoBehaviour {

    // again this only for passing further :/ 
    OnDynaKilled damaged;
	// Use this for initialization
	void Start () {
        damaged = GetComponentInChildren<OnDynaKilled>();
	}

    // from animation trigger
    void OnDieAnimationFinishHandler()
    {
        damaged.OnDieAnimationFinish();
    }
	 
}

﻿using UnityEngine;
using System.Collections.Generic;
using Enums;

public static class Globals {

    public static Dictionary<moveTypes, Vector2> vectors = new Dictionary<moveTypes, Vector2>(){
        {moveTypes.UP, Vector2.up},
        {moveTypes.DOWN, -Vector2.up},
        {moveTypes.RIGHT, Vector2.right},
        {moveTypes.LEFT, -Vector2.right},
        {moveTypes.NONE,Vector2.zero},
        {moveTypes.UNDEFINED,Vector2.zero}};    // hmm... maybe some warning here? This is sort of error.
}

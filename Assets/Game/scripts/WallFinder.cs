﻿using UnityEngine;
using System.Collections.Generic;
using Enums;
using System.Linq;
public static class WallFinder {
    public static bool isObstacleInRange(Transform t, moveTypes keyType)
    {
        if (keyType == moveTypes.UNDEFINED || keyType == moveTypes.NONE) return true;
        RaycastHit2D[] hits = Physics2D.RaycastAll(t.position, Globals.vectors[keyType],0.48f);
        return hits.Any(hit => hit.transform.tag == "wall" || hit.transform.tag == "brick"); 
    }
}

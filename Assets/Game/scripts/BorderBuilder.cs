﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using models;
public class BorderBuilder : MonoBehaviour {

    // set in inspector:
    public GameObject DownCollider;
    public GameObject UpCollider;
    public GameObject LeftCollider;
    public GameObject RightCollider;


    private float mapTileSize;
    private float borderTileSize;
    private float startX, startY;
    private MapModel mapModel;

	void Start () {
        mapTileSize = 0.48f;// mapBuilder.brick.renderer.bounds.size.x; but if I don't have Sprite in SpriteRenderer there are problems.
        borderTileSize = 2 * mapTileSize; //0.96
        startX = startY = -mapTileSize / 2 - borderTileSize / 2;
        mapModel = MapModel.GetInstance();
        buildMapBorder();
	}

    // sortingLayer == "Default" because it is lower than player layes so indescructability bubble is always above left/right walls.
    private void CreateMapTile(string spriteName, Vector2 position, string sortingLayer = "Default")
    {
        GameObject newWall = new GameObject(spriteName);
        SpriteRenderer srl = newWall.AddComponent<SpriteRenderer>();
        srl.sprite = mapModel.GetSpriteWithName(spriteName);
        newWall.transform.position = position;
        newWall.transform.rotation = Quaternion.identity;
        newWall.transform.parent = transform;
        srl.sortingLayerName = sortingLayer;
        srl.sortingOrder = (mapModel.mapHeight - (int)(newWall.transform.position.y / 0.48)) * 10;
    }

    private void buildMapBorder()
    {
        float mapWidth = mapModel.mapWidth;
        float mapHeight = mapModel.mapHeight;
        CreateMapTile("LDWallCorner", new Vector2(startX, startY), "player");
        CreateMapTile("LUWallCorner", new Vector2(startX, startY + borderTileSize + mapHeight * mapTileSize), "player");
        CreateMapTile("RDWallCorner", new Vector2(startX + mapTileSize * mapWidth + borderTileSize, startY), "player");
        CreateMapTile("RUWallCorner", new Vector2(startX + mapTileSize * mapWidth + borderTileSize, startY + mapTileSize * mapHeight + borderTileSize), "player");

        if (mapModel.worldMap == "CastleFinal")
        {
            for (int i = 0; i < mapWidth; i++)
            {
                CreateMapTile("UpWall"+Random.Range(0,2).ToString(), new Vector2(i * mapTileSize, startY + mapHeight * mapTileSize + borderTileSize),"player");
                CreateMapTile("DownWall"+Random.Range(0,2).ToString(), new Vector2(i * mapTileSize, startY),"player");
            }
        }
        else
        {
            for (int i = 0; i < mapWidth; i++)
            {
                CreateMapTile("UpWall", new Vector2(i * mapTileSize, startY + mapHeight * mapTileSize + borderTileSize),"player");
                CreateMapTile("DownWall", new Vector2(i * mapTileSize, startY),"player");
            }
        }


        if (mapModel.worldMap == "Water")
        { 
            for (int i = 0; i < mapHeight; i++)
            {
                CreateMapTile("LeftWall"+(i%2).ToString(), new Vector2(startX, i * mapTileSize));
                CreateMapTile("RightWall" + (i % 2).ToString(), new Vector2(startX + mapTileSize * mapWidth + borderTileSize, i * mapTileSize));
            }   
        }
       
        else if (mapModel.worldMap == "CastleFinal")
        {
            for (int i = 0; i < mapHeight; i++)
            {
                CreateMapTile("LeftWall" + Random.Range(0,3).ToString(), new Vector2(startX, i * mapTileSize));
                CreateMapTile("RightWall" + Random.Range(0,3).ToString(), new Vector2(startX + mapTileSize * mapWidth + borderTileSize, i * mapTileSize));
            }
        } 
        else 
        {
            for (int i = 0; i < mapHeight; i++)
            {
                CreateMapTile("LeftWall", new Vector2(startX, i * mapTileSize));
                CreateMapTile("RightWall", new Vector2(startX + mapTileSize * mapWidth + borderTileSize, i * mapTileSize));
            }
        }

        // set border colliders positions:
        DownCollider.transform.position = new Vector2((mapModel.mapWidth / 2) * mapTileSize, -mapTileSize);
        DownCollider.transform.localScale = new Vector2(((mapModel.mapWidth + 2) * 2) * mapTileSize, 1f); // +2 so this collider is a little bigger than map.

        UpCollider.transform.position = new Vector2((mapModel.mapWidth / 2) * mapTileSize, mapTileSize*mapModel.mapHeight);
        UpCollider.transform.localScale = new Vector2(((mapModel.mapWidth + 2) * 2) * mapTileSize, 1f); 

        LeftCollider.transform.position = new Vector2(-mapTileSize, mapTileSize * mapModel.mapHeight/2);
        LeftCollider.transform.localScale = new Vector2(1f, ((mapModel.mapHeight + 2) * 2) * mapTileSize);

        RightCollider.transform.position = new Vector2((mapModel.mapWidth)*mapTileSize, mapTileSize * mapModel.mapHeight / 2);
        RightCollider.transform.localScale = new Vector2(1f, ((mapModel.mapHeight + 2) * 2) * mapTileSize);

    }
}

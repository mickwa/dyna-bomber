﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleScreenMover : MonoBehaviour {

    public GameObject textImage;
    public Canvas GameMenu;

    private string moveFunctionName = "MoveTitleImage";
    private string showTextFunctionName = "ShowText";
    
    void Start()
    {
        SpriteRenderer sr = textImage.GetComponent<Renderer>() as SpriteRenderer;
        sr.color = new Color(1f, 1f, 1f, 0);
        InvokeRepeating(moveFunctionName, 0f, 0.05f);
    }

    void MoveTitleImage()
    {
        if (transform.position.y < 0)
            transform.Translate(0, 0.1f, 0);
        else
        {
            CancelInvoke(moveFunctionName);
            InvokeRepeating(showTextFunctionName, 0, 0.05f);
	    }  
    }
    private float alpha = 0;
    void ShowText()
    {
        alpha += 0.025f;
        SpriteRenderer sr = textImage.GetComponent<Renderer>() as SpriteRenderer;
        sr.color = Color.Lerp(new Color(1, 1, 1, 0f), new Color(1, 1, 1, 1f), alpha);
        if (alpha >= 1)
        {
            CancelInvoke(showTextFunctionName);
            GameMenu.transform.Translate(9f, 0f, 0f);       
        }
    }
}

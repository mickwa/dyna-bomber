﻿using UnityEngine;
using System.Collections.Generic;
using Enums;
using System;

public class RandomMove : IMoveStrategy {
    public moveTypes GetNextMove(Transform t) {
        List<moveTypes> allowedMoves = new List<moveTypes>();
         
        foreach (moveTypes move in (moveTypes[])Enum.GetValues(typeof(moveTypes)))
        {
            bool foundObstacle = WallFinder.isObstacleInRange(t, move);
            if(foundObstacle == false) allowedMoves.Add(move);
        }

        if (allowedMoves.Count == 0) return moveTypes.NONE;
            else
        return(allowedMoves[UnityEngine.Random.Range(0,allowedMoves.Count)]);
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
 
public class GameStartPositions  {
    public Vector2 Bounds;
    public Vector2 DynaStartPosition;
    public List<Vector2> BrickPositions;
    public List<Vector2> EmptyReservedPositions;
    public GameStartPositions(Vector2 start,Vector2 bounds, List<Vector2> bricks,List<Vector2> locked)
    {
        Bounds = bounds;
        DynaStartPosition = start;
        BrickPositions = bricks;
        EmptyReservedPositions = locked;
    }
}

﻿using UnityEngine;
using System.Collections;
using models;
public class MapGridGizmo : MonoBehaviour {

    public int mapWidth = 15, mapHeight = 9;
    void Start()
    {
        MapModel mapModel = MapModel.GetInstance();
        // map model does not exist in editor mode.
        mapHeight = mapModel.mapHeight;
        mapWidth = mapModel.mapWidth;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        for (int i = 0; i <= mapHeight; i++)
        {
            Vector2 start = new Vector2(transform.position.x - 0.24f, transform.position.y + i * 0.48f - 0.24f);
            Vector2 end = new Vector2(transform.position.x + 0.48f * mapWidth - 0.24f, transform.position.y + i * 0.48f - 0.24f);
            Gizmos.DrawLine(start, end);
        }
         
        for (int j = 0; j <= mapWidth; j++)
        {
            Vector2 start = new Vector2(transform.position.x + j * 0.48f - 0.24f, transform.position.y - 0.24f);
            Vector2 end = new Vector2(transform.position.x + j * 0.48f - 0.24f, transform.position.y + 0.48f * mapHeight - 0.24f);
            Gizmos.DrawLine(start, end);
        }
    }
     
}

﻿using UnityEngine;
using System.Collections;

// used when brick is destroyed
public class BrickItemSpawner : MonoBehaviour{

    // set in inspector:
    public GameObject[] items;
    
    public void CreateItem()
    {
         Object.Instantiate(items[0], Vector2.zero, Quaternion.identity);
    }
 }

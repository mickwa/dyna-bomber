﻿using UnityEngine;
using System.Collections;
using Enums;

public class key  {

    public KeyCode keycode;
    public moveTypes keyType;
    public allowedDirection direction;
    public bool allowed;    // is not allowed when there is some collision
    public bool pressed;
    public bool current;
    public int colliders;   // hold number of colliders for each key. Solves problem when Dyne obstacle collider is over many obstacles
    public key(KeyCode k, allowedDirection d,moveTypes type)
    {
        keycode = k;
        direction = d;
        keyType = type;
        SetDefaultValues();
     }
    public override string ToString()
    {
        return "KC: " + keycode +/* "   dir: " + direction +*/ "   allowed: " + allowed.ToString() + "   pressed: " + pressed.ToString()+" current: "+current+ " coll: "+colliders;
    }
    public void SetDefaultValues()
    {
        allowed = true;
        pressed = false;
        current = false;
        colliders = 0;

    }
}

﻿using UnityEngine;
using System.Collections;
using models;

/**
 *  script for camera to follow Dyna
 */
 
public class FollowDyna : MonoBehaviour {

    private Transform dyna;
    private MapModel mapModel;
    private float borderTileSize = 0.96f;
    private float minX, maxX, minY, maxY;
    private float cameraWidth, cameraHeight;
    void Start()
    {
        dyna = GameObject.FindGameObjectWithTag("Player").transform;
        cameraHeight = Camera.main.orthographicSize;
        cameraWidth = Camera.main.orthographicSize * Camera.main.aspect;
        //Debug
        mapModel = MapModel.GetInstance();
        minX = cameraWidth - borderTileSize - borderTileSize / 4;
        minY = cameraHeight - borderTileSize - borderTileSize / 4;
        maxX = borderTileSize + borderTileSize / 4 + 0.48f * (mapModel.mapWidth - 1) - cameraWidth; // -1 because borderTileSize/4 is added
        maxY = borderTileSize + borderTileSize / 4 + 0.48f * (mapModel.mapHeight - 1) - cameraHeight; // -1 because borderTileSize/4 is added
    }

	void Update () {
        if (dyna != null)
        {
            float newX = Mathf.Clamp(dyna.transform.position.x, minX, maxX);
            float newY = Mathf.Clamp(dyna.transform.position.y, minY, maxY);
            transform.position = new Vector3(newX, newY, -10f);
        }
	}
    IEnumerator WaitForDynaToSpawn()
    {
        GameObject tmp= GameObject.FindGameObjectWithTag("Player");
        if (tmp == null)
            yield return null;
        else
        {
            dyna = tmp.transform;
            yield break;
        }
    }
}

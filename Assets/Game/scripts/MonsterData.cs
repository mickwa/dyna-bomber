﻿using UnityEngine;
using System.Collections;

public static class MonsterData {
    private static int _killedMonsters = 0;
    private static int _monstersToKill = 0;

    public static int killedMonsters { private set{} get { return _killedMonsters; } }
    public static int monstersToKill { private set{} get { return _monstersToKill; } }
    public static void KilledMonster(string name)
    {
        Debug.Log("killed name: " + name);
         _killedMonsters++;
         DisplayMonsterStats();
         if (KilledThemAll())
         {
             MapBuilder2 mapBuilder = GameObject.FindGameObjectWithTag("map").GetComponent<MapBuilder2>();
             mapBuilder.SpawnLevelExit();
         }
    }

    public static bool KilledThemAll()
    {
        return (_killedMonsters >= _monstersToKill);
    }

    public static void AddMonster()
    {
        _monstersToKill++;
        DisplayMonsterStats();
    }

    public static void ResetData()
    {
        Debug.Log("Monster data reset...");
        _killedMonsters = 0;
        _monstersToKill = 0;
    }

    public static void DisplayMonsterStats()
    {
        Debug.Log("Stats: " + _killedMonsters + " / " + _monstersToKill+ " killed/spawned");
    }
}